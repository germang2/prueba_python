from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from .models import TipoUsuario

# Create your views here.
def register(request):
  context = {}
  if request.method == "POST":
    # Checks is email already exist
    if len(User.objects.filter(email = request.POST.get('email'))) > 0:
      context['user_exist'] = "Ya se encuentra registrado el correo " + str(request.POST.get('email')) + ", por favor ingresa otro"
    elif len(request.POST.get('password')) < 6:
      context['password'] = "Contraseña muy corta, se esperan minimo 6 caracteres"
    else:
      # Put the email in all required files
      user = User(
            username = request.POST.get('email'),
            first_name = request.POST.get('email'),
            email = request.POST.get('email')
            )
      user.set_password(request.POST.get('password'))
      user.save()
      tipo_usuario = TipoUsuario(user = user)
      tipo_usuario.save()
      context['sucessfull'] = "Usuario registrado con exito"
  return render(request, 'users/register.html', context)

def loginin(request):
  context = {}
  if request.method == "POST":
    email = request.POST['email']
    password = request.POST['password']
    user = authenticate(request, username=email, password=password)
    if user is not None:
      login(request, user)
      return redirect('/')
    else:
      context['error_login'] = "El usuario o la contraseña no coincide"
  return render(request, 'users/login.html', context)

@login_required
def index(request):
  context = {}
  return render(request, 'index.html', context)