from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class TipoUsuario(models.Model):
  user = models.ForeignKey(
    User,
    on_delete=models.CASCADE
  )
  tipo = models.CharField(max_length=50, default="cliente")

  def __str__(self):
    return str(self.user.email) + " - " + str(self.tipo)
