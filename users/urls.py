from django.contrib import admin
from django.urls import path
from .views import register, loginin

app_name = "users"
urlpatterns = [
    path('register', register, name="register"),
    path('login', loginin, name="login"),
] 