from django.urls import path
from .views import venta, reportes

app_name = "ventas"
urlpatterns = [
    path('nuevolibro', venta, name="venta"),
    path('reportes', reportes, name="reportes"),
] 