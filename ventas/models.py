from django.db import models
from django.contrib.auth.models import User
from libros.models import libro

# Create your models here.
class Venta(models.Model):
  user = models.ForeignKey(
    User,
    on_delete=models.CASCADE
  )
  libro = models.ForeignKey(
    libro,
    on_delete=models.CASCADE
  )
  fecha_hora = models.DateTimeField(auto_now=True) 
  cantidad = models.PositiveIntegerField()
  total = models.FloatField(default=0.0)

  def __str__(self):
    return str(self.user.email) + " -> " + str(self.libro.titulo)