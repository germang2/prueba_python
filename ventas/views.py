from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from libros.models import libro
from django.shortcuts import redirect, reverse
from .models import Venta
from django.contrib.auth.models import User

# Create your views here.
@login_required
def venta(request):
  context = {}
  usuario= request.user
  if request.method == "POST":
    lista_carrito = request.POST.dict()
    del lista_carrito['csrfmiddlewaretoken'] 
    lista_carrito = list(lista_carrito)
    for item in lista_carrito:
      cantidad_vendida = request.POST.get(item)
      try:
        cantidad_vendida = int(request.POST.get(item))
      except:
        pass
      book = libro.objects.get(id = int(item))
      book.disponibles = book.disponibles - cantidad_vendida
      book.save()
      nueva_venta = Venta(
        user = usuario,
        libro = book,
        cantidad = cantidad_vendida,
        total = int(cantidad_vendida) * book.precio 
      )
      nueva_venta.save()
    context['sucessfull'] = "Hemos recibido su solicitud y será procesada por nuestros agentes. Gracias por su compra"
  return render(request, 'index.html', context)

@login_required
def reportes(request):
  context = {}
  usuarios = User.objects.all()
  lista_ventas = []
  for usuario in usuarios:
    try:
      ventas = Venta.objects.filter(user = usuario)
      if len(ventas)>0:
        lista_ventas.append([usuario, ventas])
    except:
      continue

  context['ventas'] = lista_ventas
  print(lista_ventas[0][1])
  return render(request, 'admin/reportes.html', context)