from django.contrib import admin
from django.urls import path
from .views import addBook

app_name = "libros"
urlpatterns = [
    path('nuevolibro', addBook, name="addBook"),
] 