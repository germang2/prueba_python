# Generated by Django 2.0 on 2018-06-05 18:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('libros', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='libro',
            name='imagen',
            field=models.ImageField(blank=True, upload_to='images/'),
        ),
    ]
