from django.db import models

# Create your models here.
class libro(models.Model):
  titulo = models.CharField(max_length=100)
  precio = models.FloatField()
  disponibles = models.PositiveIntegerField()
  imagen = models.ImageField(upload_to='images/', blank = True)

  def __str__(self):
    return str(self.titulo)
