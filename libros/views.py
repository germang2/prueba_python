from django.shortcuts import render
from .models import libro
from django.contrib.auth.decorators import login_required
from users.models import TipoUsuario
from django.shortcuts import redirect, reverse

# Create your views here.
@login_required
def addBook(request):
  usuario = request.user
  #tipo_usuario = TipoUsuario.objects.get(user = usuario)
  if (not usuario.is_staff):
    return redirect(reverse('index'))
  context = {}
  if request.method == "POST":
    upload_file = ""
    try:
      upload_file = request.FILES['imagen']
      # for validate extensio of imagen, firts separate the image name using .
      extension = str(upload_file).split(".")
      # then get only the extension
      extension = extension[len(extension) - 1]
      if (extension != "png" and extension != "jpg" and extension != "jpeg"):
        context['imagen_error'] = "La imagen debe ser extension .png, .jpg o jpeg"
    except:
      pass
      
    if int(request.POST.get('precio')) < 0:
      context['precio_error'] = "El precio no puede ser menor a 0"
    elif int(request.POST.get('disponible')) < 0:
      context['disponible_error'] = "La cantidad de libros disponibles no pueden ser menor a 0"
    else:
      book = libro(
        titulo = request.POST.get('titulo'),
        precio = request.POST.get('precio'),
        disponibles = request.POST.get('disponible'),
        imagen = upload_file
      )
      book.save()
      context['sucessfull'] = "Libro añadido con éxito"

  return render(request, 'admin/libro.html', context)

@login_required
def index(request):
  context = {}
  libros = libro.objects.all()
  context['libros'] = libros  
  return render(request, 'index.html', context)

@login_required
def carrito(request):
  context = {}
  libros = []
  if request.method == "GET":
    context['cancelacion'] = "Su compra se ha cancelado"
    return render(request, 'carrito.html', context)

  if request.method == "POST":
    lista_carrito = request.POST.dict()
    # remove the csrf_token of list
    del lista_carrito['csrfmiddlewaretoken'] 
    lista_carrito = list(lista_carrito)
    print(lista_carrito)
    valor_compra = 0.0
    for item in lista_carrito:
      cantidad = request.POST.get(item)
      try:
        cantidad = int(request.POST.get(item))
      except:
        pass
      if (cantidad > 0):
        book = libro.objects.get(id = int(item))
        total = cantidad * book.precio
        valor_compra += total
        libros.append([book, cantidad, total])
    context['libros'] = libros
    context['valor_compra'] = valor_compra
    
  return render(request, 'carrito.html', context)